/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')({
    pattern: ['gulp-*']
});

/**
 * Variables
 */
var srcDir = './app/styles';
var entryFiles = [srcDir + '/vendors.scss', srcDir + '/styles.scss'];

var tmpDir = './.tmp/styles';
var dstDir = './dist';
var targetFile = 'styles.css';

var sassParams = {
    errLogToConsole: true,
    sourceMap: 'sass',
    sourceComments: 'map',
    precision: 10,
    style: 'expanded'
};

/**
 * @param minify
 * @param dest
 * @returns {*}
 */
function buildStyles(minify, dest) {
    var b = gulp.src(entryFiles)
        .pipe($.plumber(function(error) {
            $.util.log($.util.colors.red(error.message));
            $.util.log($.util.colors.red(error.stack));
            // prevent task crash on error
            this.emit('end');
        }))
        .pipe($.concat(targetFile))
        .pipe($.sass(sassParams))
        .pipe($.autoprefixer(/*'last 1 version, ie 10, ie 11'*/));

    if(minify) {
        b = b.pipe($.cssmin());
    }

    b.pipe(gulp.dest(dest));
    return b;
}

/**
 * @type {{build: module.exports.build, watch: module.exports.watch}}
 */
module.exports = {
    build: function() {
        return buildStyles(true, dstDir);
    },
    watch: function(cb) {
        buildStyles(false, tmpDir).on('end', cb);
        $.watch(srcDir + '/**/*.scss', { ignoreInitial: true }, function() {
            buildStyles(false, tmpDir);
        });
    }
};