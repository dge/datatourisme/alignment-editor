/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var angular = require('angular');

var endpoints = {};

// service definition
angular
    .module('alignment-editor')
    .service('apiEndpoints', apiEndpoints);

// requires
var AlignmentDefinition = require("./models/AlignmentDefinition");
var XpathSchema = require("./models/XpathSchema");
var Ontology = require('ontology-js');

/**
 * @ngInject
 */
function apiEndpoints($injector) {
    return {
        ontology: $injector.invoke(ontologyEndpoint),
        xpath: $injector.invoke(xpathEndpoint),
        alignment: $injector.invoke(alignmentEndpoint),
        stats: $injector.invoke(statsEndpoint),
        preview: $injector.invoke(previewEndpoint)
    };
}

/**
 * Ontology endpoint
 *
 * @ngInject
 */
function ontologyEndpoint($http, appConfig) {
    var promise = null;

    return {
        load: load
    };

    function load() {
        if(!promise) {
            promise = $http.get(appConfig.endpoints.ontology, {cache: true}).then(function(response) {
                var ontology = Ontology.fromJson(response.data);
                ontology.PointOfInterest = ontology.getClass(appConfig.datatourisme.namespace + 'PointOfInterest');
                ontology.PointOfInterestClass = ontology.getClass(appConfig.datatourisme.namespace + 'PointOfInterestClass');
                return ontology;
            });
        }
        return promise;
    }
}

/**
 * Xpath endpoint
 *
 * @ngInject
 */
function xpathEndpoint($http, appConfig) {
    var promise = null;

    return {
        load: load
    };

    function load(type) {
        if(!promise) {
            var url = appConfig.endpoints.xpath + '/' + type.split(":").pop();
            promise = $http.get(url, {cache: true}).then(function(response) {
                return new XpathSchema(response.data);
            });
        }
        return promise;
    }
}

/**
 * Alignment endpoint
 *
 * @ngInject
 */
function alignmentEndpoint($http, appConfig, $injector) {
    return {
        load: load,
        save: save
    };

    function load(ontology) {
        if(!ontology) {
            // if no ontology, load it through endpoint
            return $injector.get("apiEndpoints").ontology.load().then(function(ontology) {
                return load(ontology);
            });
        }
        return $http.get(appConfig.endpoints.alignment, {cache: true}).then(function(response) {
            return new AlignmentDefinition(response.data, ontology);
        });
    }

    function save(definition) {
        console.log("save definition");
        return $http.post(appConfig.endpoints.alignment, definition.toJson());
    }
}

/**
 * Preview endpoint
 *
 * @ngInject
 */
function previewEndpoint($http, appConfig) {
    return {
        index: index,
        preview: preview
    };

    function index(params) {
        return $http({
            method: "GET",
            url: appConfig.endpoints.preview,
            params: params
        }).then(function(response) {
            return response.data;
        });
    }

    function preview(id, alignment) {
        return $http({
            method: "POST",
            url: appConfig.endpoints.preview + '/' + id,
            data: alignment
        }).then(function(response) {
            return response.data;
        });
    }
}

/**
 * Preview endpoint
 *
 * @ngInject
 */
function statsEndpoint($http, appConfig) {
    return {
        stats: stats
    };

    function stats() {
        return $http.get(appConfig.endpoints.stats, {cache: true}).then(function(response) {
            return response.data;
        });
    }
}