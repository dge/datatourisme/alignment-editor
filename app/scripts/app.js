/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

window.jQuery = window.$ = require('jquery');
var angular = require('angular');

// vendors
require('angular-ui-router');
require('nanoscroller');
window.Selectize = require('selectize');
require('angular-selectize2/dist/selectize');
require('ngstorage');
require('bootstrap');
require('bootstrap-notify');
require('ngFileReader');
require('xpath-editor');
require('./vendors/*', {mode: 'expand'});

// module definition
var app = angular.module('alignment-editor', [
    // angular modules
    // 3rd Party modules
    'ui.router',
    'xpath-editor',
    'ngDialog',
    'selectize',
    'sun.scrollable',
    'ngStorage',
    'ngFileReader'
]);

// appConfig
require("../../config/config.yml", {mode: function(base, files) {
    if(files.length > 0) {
        return "app.constant('appConfig', require('" + files[0] + "'));";
    }
    return "";
}});

app.config(config);
app.run(run);

// components
require('./components/*/*.js', {mode: 'expand'});
require('./directives/*/*.js', {mode: 'expand'});
require('./filters/*/*.js', {mode: 'expand'});
require('./services/*/*.js', {mode: 'expand'});
require('./states/*/*.js', {mode: 'expand'});

var jQuery = require("jquery");

/**
 * @ngInject
 */
function config($locationProvider, $httpProvider, $urlRouterProvider, $sessionStorageProvider) {
    //$locationProvider.html5Mode({enabled: false, requireBase: false});
    $urlRouterProvider.otherwise("/");
    $sessionStorageProvider.setKeyPrefix('datatourisme.alignmentEditor');
    $httpProvider.interceptors.push(function ($q) {
        return {
            'responseError': function (rejection) {
                var msg = rejection.data.error || "Erreur inconnue.";
                jQuery.notify(msg, {type: "danger", z_index: 9999, delay: 0});
                return $q.reject(rejection);
            }
        };
    });
}

/**
 * @ngInject
 */
function run($rootScope, $state) {
    $rootScope.keys = Object.keys;
}
