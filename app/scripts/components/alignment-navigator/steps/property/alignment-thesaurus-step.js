/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var angular = require('angular');

angular
    .module('alignment-editor')
    .component('alignmentThesaurusStep', {
        template: require("./alignment-thesaurus-step.html"),
        controller: controller,
        require: {
            navigator: "^alignmentNavigator"
        },
        bindings: {
            step: "<"
        }
    });

/**
 * @ngInject
 */
function controller($controller, $scope) {
    angular.extend(this, $controller('StepFormController', {$scope: $scope}));

    this.options = [];
    this.config = {
        optgroupField: 'group',
        optgroupValueField: 'uri',
        optgroupLabelField: 'label',
        searchField: ['label'],
        valueField: 'uri',
        labelField: 'label',
        placeholder: 'Sélectionnez une valeur ou plusieurs valeurs du thésaurus',
        optgroups: []
    };

    this.init = function () {

        if(this.step.property.functional) {
            this.config.maxItems = 1;
            this.config.placeholder = 'Sélectionnez une valeur du thésaurus';
        }

        var ranges = this.step.property.ranges;
        var options = [];
        var optgroups = {};

        for(var i=0; i<ranges.length; i++) {
            var individuals = ranges[i].getIndividuals(false);
            for(var j=0; j<individuals.length; j++) {
                var individual = individuals[j];
                var group = individual.getType();
                options.push({
                    uri: individual.getUri(),
                    label: individual.getPreferedLabel(),
                    group: group.getUri()
                });

                if(!optgroups[group.getUri()]) {
                    optgroups[group.getUri()] = {
                        uri: group.getUri(),
                        label: group.getPreferedLabel()
                    };
                }
            }
        }

        var optgroupsArr = [];
        for (var key in optgroups) {
            optgroupsArr.push(optgroups[key]);
        }
        this.config.optgroups = optgroupsArr;

        options = options.sort(sortByLabel);
        this.options = options;
    };
}

/**
 * @param a
 * @param b
 * @returns {number}
 */
function sortByLabel(a, b) {
    if(a.label < b.label) return -1;
    else if(b.label < a.label) return 1;
    return 0;
}