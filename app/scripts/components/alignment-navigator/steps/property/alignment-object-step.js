/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var angular = require('angular');

angular
    .module('alignment-editor')
    .component('alignmentObjectStep', {
        template: require("./alignment-object-step.html"),
        controller: controller,
        require: {
            navigator: "^alignmentNavigator"
        },
        bindings: {
            step: "<"
        }
    });

/**
 * @ngInject
 */
function controller($controller, $window, $scope) {

    angular.extend(this, $controller('StepFormController', {$scope: $scope}));

    /**
     * Default model to def function
     *
     * @param data
     */
    var parent_modelToDef = this.modelToDef;
    this.modelToDef = function(data) {
        if(data && data.type && data.type == 'single') {
            return {type: 'single'};
        }
        return parent_modelToDef(data);
    };

    /**
     * @returns {boolean}
     */
    this.init = function() {
        if (this.isFunctional() && (!this.data || !this.data.type)) {
            this.data =  angular.extend(this.data ? this.data : {}, {type: 'single'});
        }
    };

    /**
     * @returns {boolean}
     */
    this.isFunctional = function() {
        return !!this.step.property.functional;
    };

    /**
     * @returns {boolean}
     */
    this.confirmContextChange = function(key) {
        if(this.navigator.hasDefinitionRules(this.step.path)) {
            var confirm = this.original[key] && !angular.equals(this.data[key], this.original[key]);
            if(confirm && !$window.confirm("Attention : le changement de contexte va invalider l'ensemble de l'arborescence des règles. Êtes-vous sûr de vouloir continuer ?")) {
                this.reset();
            }
        }
    }
}