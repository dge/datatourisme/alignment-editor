/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var angular = require('angular');

// module definition
angular.module('alignment-editor')
    .controller('StepFormController', StepFormController);

/**
 * @ngInject
 */
function StepFormController($scope, $timeout) {
    this.data = null;
    this.original = null;

    /**
     * Default def to model function
     *
     * @param def
     */
    this.defToModel = function(def) {
        var type = def.type == "xpath" || def.type == "xquery" ? "extract" : def.type;
        var data = { type: type };
        def = angular.copy(def);

        if(def.rules) {
            //data.rules = angular.copy(def.rules)
            delete def.rules;
        }

        data[type] = def;
        return data;
    };

    /**
     * Default model to def function
     *
     * @param data
     */
    this.modelToDef = function(data) {
        var def = null;
        if(data) {
            var type = data.type;
            if(type && data[type]) {

                if(type == "constant" && !data[type].value) {
                    return null;
                }

                def = angular.extend({type: data.type}, data[type]);
                // if(data.rules) {
                //     def.rules = data.rules;
                // }
            }
        }
        return def;
    };

    /**
     * Optional init
     */
    this.init = function() {
        // to overwrite if needed
    };

    /**
     * Optional precommit (return undefined to undo)
     *
     * @param data
     * @param old
     * @returns {*}
     */
    this.onPreCommit = function(data, old) {
        return data;
    };

    /**
     * Commit
     *
     * @param data
     */
    this.commit = function(data) {
        this.original = data;
        var def = this.modelToDef(data);
        this.navigator.setDefinitionRule(this.step.path, def);
    };

    /**
     * Handle watched changes
     */
    this.handleChanges = function(data) {
        // check equality
        if(angular.equals(data, this.original)) {
            return;
        }
        // force navigator to get back to the step
        this.navigator.navigateTo(this.step);
        var that = this;
        $timeout(function() {
            // check form validity
            if(that.form.$valid) {
                data = that.onPreCommit(angular.copy(data), that.original);
                if(data !== undefined) {
                    that.commit(data);
                }
            }
        });
    };

    /**
     * Reset original values
     */
    this.reset = function() {
        for(var key in this.data) {
            this.data[key] = this.original[key];
        }
    };

    /**
     * Main initialization function
     */
    this.$onChanges = function () {
        this.xpathContext = this.navigator.getDefinitionXpathContext(this.step.path);
        var def = this.navigator.getDefinitionRule(this.step.path);
        this.data = def ? this.defToModel(def) : {};
        this.original = angular.copy(this.data);
        this.init();

        // watch data to commit
        var that = this;
        $scope.$watch(function() {
            return that.data
        }, function(val, old) {
            that.handleChanges(val, old);
        }, true);
    };
}
