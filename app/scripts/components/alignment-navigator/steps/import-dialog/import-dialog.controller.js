/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var angular = require('angular');
var PrefixMapping = require("../../../../services/api-endpoints/models/PrefixMapping");

// module definition
angular.module('alignment-editor')
    .controller('ImportDialogController', ImportDialogController);

/**
 * @ngInject
 */
function ImportDialogController($scope, navigator, step) {
    $scope.readMethod = "readAsText";
    $scope.file = null;
    $scope.data = null;
    $scope.loading = false;
    $scope.applicableSteps = [];
    $scope.form = null;
    $scope.options = {
        method: 'append'
    };

    /**
     * @param e
     * @param file
     */
    $scope.onReaded = function( e, file ){
        $scope.file = file;
        $scope.data = null;
        $scope.applicableSteps = [];
        try {
            $scope.data = JSON.parse(e.target.result);
            var prefixMap = new PrefixMapping($scope.data['@context']);
            $scope.applicableSteps = getApplicableSteps(step.classes, $scope.data.rules, prefixMap);
        } catch(error) {

        }
    };

    /**
     * @param e
     * @param file
     */
    $scope.submit = function() {
        $scope.loading = true;
        var method = $scope.options.method;

        if($scope.applicableSteps.length == 0) {
            return;
        }

        var rules = angular.copy(navigator.getDefinition().getRules(step.path));
        if(method == "reset") {
            // reset : empty the whole rules object
            rules = {};
        }

        function process(steps, rules) {
            for(var i=0; i<steps.length; i++) {
                var step = steps[i];
                var uri = navigator.getDefinition().normalizeUri(step.property.getUri());
                if(rules[uri] && method == "append") {
                    continue;
                }

                var rule = angular.copy(step.rule);

                // keep existant rules if not reset
                // if(rules[uri] && rules[uri].rules && method != "reset") {
                //     rule.rules = angular.copy(rules[uri].rules);
                // }

                if(step.steps && step.steps.length > 0) {
                    rule.rules = rule.rules ? rule.rules : {};
                    process(step.steps, rule.rules);
                }

                rules[uri] = rule;
            }
        }

        process($scope.applicableSteps, rules);

        if(Object.keys(rules).length > 0) {
            navigator.getDefinition().setRules(step.path, rules);
            jQuery.notify("Les règles ont correctement été importées.", {type: "success", z_index: 9999});
            $scope.closeThisDialog();
        } else {
            jQuery.notify("Aucune règle n'a pu être importée.", {type: "warning", z_index: 9999});
        }

        $scope.loading = false;
    };

    /**
     *
     * @param steps
     * @returns {*}
     */
    $scope.recursiveStepCount = function(steps) {
        var count = steps.length;
        for(var i=0; i<steps.length; i++) {
            if(steps[i].steps) {
                count = count + $scope.recursiveStepCount(steps[i].steps);
            }
        }
        return count;
    };

    /**
     * get applicable keys for the given step
     */
    function getApplicableSteps(classes, rules, prefixMap) {
        //var steps = step.steps ? step.steps : navigator.getSteps(step.property.getRanges(), step.path);

        var properties = {};
        for(var i=0; i<classes.length; i++) {
            var ranges = classes[i].getCandidateProperties();
            for(var j=0; j<ranges.length; j++) {
                properties[ranges[j].getUri()] = ranges[j];
            }
        }

        var applicableSteps = [];
        for(var uri in rules) {
            var property = properties[prefixMap.expandPrefix(uri)];
            if (property) {
                var step = {
                    property: property,
                    rule: rules[uri]
                };
                if(rules[uri].rules && property.getRanges() && property.getRanges().length) {
                    step.steps = getApplicableSteps(property.getRanges(), rules[uri].rules, prefixMap);
                }
                delete step.rule.rules;
                applicableSteps.push(step);
            }
        }

        return applicableSteps;
    }
}