/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var angular = require('angular');

// module definition
angular.module('alignment-editor')
    .controller('AppController', AppController);

/**
 * @ngInject
 */
function AppController($scope, appConfig, debounce, ontology, definition, stats, apiEndpoints) {
    var endpoint = apiEndpoints.alignment;
    $scope.appConfig = appConfig;
    $scope.ontology = ontology;
    $scope.definition = definition;
    $scope.debug = appConfig.debug;
    $scope.stats = stats;

    var saveDefinition = debounce(1000, function () {
        endpoint.save(definition);
    });

    $scope.$watch(function() { return definition.getRule(); }, function(data, old) {
        if(!angular.equals(data, old)) {
            saveDefinition();
        }
    }, true);
}